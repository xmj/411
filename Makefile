# $FreeBSD$

PORTNAME=	411
PORTVERSION=	1.1.0
DISTVERSIONPREFIX=	v
CATEGORIES=	www

MAINTAINER=	jhixson@gmail.com
COMMENT=	Alert Management Web Application

LICENSE=	MIT

RUN_DEPENDS=	apache24>=2.4.0:www/apache24 \
				sqlite3>=3.3:databases/sqlite3 \
				php-composer>=1.0.0:devel/php-composer \
				elasticsearch2>=2.0:textproc/elasticsearch2

NO_ARCH=	yes
NO_BUILD=	yes

USES=	php
USE_PHP=	sqlite3

USE_GITHUB=	yes
GH_ACCOUNT=	etsy
GH_TAGNAME=	1612c1e

do-install:
	@${MKDIR} ${STAGEDIR}${WWWDIR}/${PORTNAME}
	(cd ${WRKSRC}/ && ${COPYTREE_SHARE} "*" ${STAGEDIR}${WWWDIR})

.include <bsd.port.mk>
